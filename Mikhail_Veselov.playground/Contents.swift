import UIKit

//Task_1.1
var fio = "Mikhail Veselov"
print (fio)

//Task_2.1
var milkmanPhrase = "Молоко - это полезно"
print (milkmanPhrase)

//Task_2.2
//let milkPrice: Int = "Три рубля"
let milkPrice: Int = 3
print ("\(milkPrice) рубля")

//Task_2.3
//let milkPrice: Int = 3
let milkPrice2: Double = 4.20
print ("\(milkPrice2) рубля")

//Task_2.4
let milkPrice3: Double = 4.20
var milkBottleCount: Int? = nil
var profit: Double = 0.0
milkBottleCount = 20
profit = milkPrice3 * Double(milkBottleCount!)
print (profit)

//Task_2.4 **
let milkPrice4: Double = 4.20
var milkBottleCount2: Int? = nil
var profit2: Double = 0.0
milkBottleCount2 = 30
profit2 = milkPrice4 * Double(milkBottleCount2!)
if milkBottleCount2 != nil {
   print (profit2)
} else {
    print ("Значение milkBottleCount2 == nil")
}
//использование force unwrap без обработки вызывает ошибку и приводит к завершению выполнения программы без какого-либо вменямого результат


//Task_2.5
var employeesList = [String]()
employeesList = [
    "Геннадий Секретарь",
    "Петр Курьер",
    "Андрей Грузчик",
    "Марфа Бухгалтер",
    "Иван Лактозов Веселый молочник"
]
print (employeesList)

//Task_2.6
var isEveryoneWorkHard: Bool = false
var workingHours = 39
if workingHours >= 40 {
    isEveryoneWorkHard = true
} else if workingHours <= 40 {
    isEveryoneWorkHard = false
}
print (isEveryoneWorkHard)
