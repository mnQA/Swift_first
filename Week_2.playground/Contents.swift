import UIKit
import Foundation

func addTwoInts (a: Int, b: Int) -> Int {
    return a + b
}

func multiplyTwoints (a: Int, b: Int) -> Int {
    return a * b
}
//--------------------------------------------------------------------------
var mathFunction = addTwoInts
print ("Результат: \(mathFunction (5, 3))")

var mathFunction2 = multiplyTwoints
print ("Result: \(mathFunction2 (4, 12))")
//--------------------------------------------------------------------------
func printResualt(of mathFunction: (Int, Int) -> Int, a: Int, b: Int) {
    print("Result: \(mathFunction(a, b))")
}

printResualt(of: addTwoInts, a: 3, b: 7)
//--------------------------------------------------------------------------
func chooseStepFunction(backward: Bool) -> (Int) -> Int {
    
    func stepForward(input: Int) -> Int {
        return input + 1
    }
    
    func stepBackward(input: Int) -> Int {
        return input - 1
    }
    
    return backward ? stepBackward : stepForward
}

var currentValue = 4
let moveNearerToZero = chooseStepFunction (backward: currentValue > 0)
while currentValue != 0 {
    print ("\(currentValue)... ")
    currentValue = moveNearerToZero (currentValue)
}
print ("Zero!")
//--------------------------------------------------------------------------
//Subtask_1

func squareRootDown (a: Double) -> Double {
    return round(sqrt(a))
}
print ("\(squareRootDown(a: 3.33))")
//--------------------------------------------------------------------------
//Subtusk_2

func sumEvenNumbers (a: Int, b: Int) -> Int {
    if a%2 == 0 && b%2 == 0 {
        return a + b
    } else if a%2 != 0 && b%2 != 0 {
        return 0
    } else if a%2 != 0 {
        return a
    } else {
        return b
    }
}
print (sumEvenNumbers(a: 2, b: 3))
//--------------------------------------------------------------------------
//Subtask_3

func printTemperature (celsium: Double) -> (celsiumToKelvin: Double, celsiumToFahrenheit: Double) {
    let celsiumToKelvin = celsium + 273.15
    let celsiumToFahrenheit = (celsium * 9/5) + 32
    return (celsiumToKelvin, celsiumToFahrenheit)
}
var temperature = printTemperature(celsium: 2)
print ("\(temperature.celsiumToFahrenheit) градусов Фаренгейта \n\(temperature.celsiumToKelvin) градусов Кельвина")
//--------------------------------------------------------------------------
//Subtask_4

func sumMaxInt() -> UInt {
    return UInt(Int.max) + UInt(Int.max)
}
print (sumMaxInt())

//--------------------------------------------------------------------------
//Subtask_5
//func smallestEvenMultiple {
//
//}

//--------------------------------------------------------------------------
//Subtask_6
let massive = [1, 2, 5, 9, 4, 6, 10, 4, 0]
let searchValue = 4
//var currentIndex = 0
for number in massive {
    if number == searchValue {
        print ("есть")
        break
    }
    //currentIndex += 1
}

//--------------------------------------------------------------------------
//Subtask_7
var massive2 = [1, 2, 3, 4, 5, 6, 7]
//massive2.count
//massive2[massive2.count - 1]
//massive2[massive2.endIndex - 1]
//var currentIndex2 = 0
for _ in massive2 {
    print ("-\(massive2.map { String($0) }.joined(separator: "-"))-")
    break
    }


let array = ["1", "2", "3"]
print("массив: \(array.joined(separator: " "))")

let array2 = [1, 2, 3]
print("массив: \(array2.map { String($0) }.joined(separator: " "))")

//--------------------------------------------------------------------------
//Subtask_8
//Дано число n=1000. Его нужно разделить на 2 столько раз, пока результат деления не станет меньше 50.
//
//Какое число получится?
//Посчитай количество итераций, необходимых для этого (итерация — это проход цикла).
//Запиши количество итераций в переменную num.
//let n = 1000
//let nCount = n / 2
//var num = numCount
//for numCount in nCount {
//    print ("\(index) ghbdtn \(index / 20)")
//}

//Задача 1
//Напиши замыкание, которое будет печатать следующие приветствие “Hello, {user}!”, где {user} — аргумент замыкания.
var printing: () -> Void = {
    print ("Hello, \(user)")
}
var user = "Oleg"
printing ()

var printing1 =  { (user: String) in
    return ("Hello, \(user)")
}
printing1("Oleg")

// Замыкание с 2 параметрами Имя Фамилия

var printing3 = { (name: String, subname: String) in
    print ("Hello, \(name) \(subname)")
}
printing3 ("asd", "qwe")
//Задача 2
//Если переменная n больше нуля, то в замыкание printout запиши функцию, которая выводит “!”, иначе запиши функцию, которая выводит “!!”.
var printOut = { (n: Int) in
    if n > 0 {
        print ("!")
    } else {
        print ("!!")
    }
}
printOut(-3)

//Задача 3
//Создай функцию mathOperation, которая принимает 2 параметра: число и замыкание, которое возводит число в куб. Результатом функции является сложение числа и выполнение замыкания от этого числа.

//func mathOperation (i: Decimal, x: Decimal) -> (y: Decimal) = { a, b -> Decimal in
//        return pow(i, 3)
//    }
//}

func cub (d: Decimal) -> (Decimal) {
    return pow(d, 3)
}
print (cub(d: 3))
//Реализуй строковый буфер на основе функции, со следующим синтаксисом:
//
//Создание переменной с замыканием: var buffer = makeBuffer().
//Вызов makeBuffer() должен возвращать такую функцию buffer, которая:
//при вызове buffer(“value”) добавляет значение в некоторое внутреннее хранилище.
//при вызове с пустым аргументом buffer("") при вызове с пустым аргументом buffer("") выводит значение внутреннего хранилища в консоль.
//
var myStr = "123_4"
print(myStr.count)

func makeBuffer() -> (String) -> Void {
    var buffer = ""
      return { value in
        if (value.count == 0) {
          print(buffer)
        } else {
            buffer += value
        }
      }
    }
var buffer = makeBuffer()
buffer("Замыкание")
buffer(" использовать")
buffer(" нужно!")
buffer("") // Замыкание использовать нужно!


//Сделай функцию checkPrimeNumber, которая:
//
//Принимает целое число.
//Проверяет, является ли число простым.
//В результате возвращает булево значение.

func checkPrimeNumber (a: Int) -> Bool {
    if a <= 2 {
        return true
    }
    for x in 2 ..< a {
            if a % x == 0 {
                return false
            }
    }
    return true
}

checkPrimeNumber (a: 2)
